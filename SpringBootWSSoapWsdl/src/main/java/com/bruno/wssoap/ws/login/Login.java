//
// Questo file è stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.3.0 
// Vedere <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Qualsiasi modifica a questo file andrà persa durante la ricompilazione dello schema di origine. 
// Generato il: 2019.01.23 alle 04:16:07 PM CET 
//


package com.bruno.wssoap.ws.login;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import ws.login.beans.LoginServiceInputBean;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="loginServiceInputBean" type="{http://beans.login.ws}LoginServiceInputBean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "loginServiceInputBean"
})
@XmlRootElement(name = "login")
public class Login {

    @XmlElement(required = true)
    protected LoginServiceInputBean loginServiceInputBean;

    /**
     * Recupera il valore della proprietà loginServiceInputBean.
     * 
     * @return
     *     possible object is
     *     {@link LoginServiceInputBean }
     *     
     */
    public LoginServiceInputBean getLoginServiceInputBean() {
        return loginServiceInputBean;
    }

    /**
     * Imposta il valore della proprietà loginServiceInputBean.
     * 
     * @param value
     *     allowed object is
     *     {@link LoginServiceInputBean }
     *     
     */
    public void setLoginServiceInputBean(LoginServiceInputBean value) {
        this.loginServiceInputBean = value;
    }

}
