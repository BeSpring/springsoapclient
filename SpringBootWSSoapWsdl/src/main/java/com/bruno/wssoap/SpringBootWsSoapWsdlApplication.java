package com.bruno.wssoap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootWsSoapWsdlApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootWsSoapWsdlApplication.class, args);
	}

}

